/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**********************************************************************
              				Header Files
 ***********************************************************************/

#include "eye.h"

G_DEFINE_TYPE (EyeVideoPlayer, eye, CLUTTER_TYPE_ACTOR)
#define EYE_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), EYE_TYPE_VIDEO_PLAYER, EyeVideoPlayerPrivate))

guint  videoplayer_debug_flags = 0;
gchar *pURL = NULL;
/**********************************************************************
              				Function Prototype Start
 ***********************************************************************/

static void initialize_client_handlers(ClutterActor* actor);

/**********************************************************************
              				Function Prototype End
 ***********************************************************************/

EyeVideoPlayer *defaultVideoPlayer = NULL;
ClutterActor *pVideoPlayerStage = NULL;



static void video_player_termination_handler (int signum)
{
	gchar *pCurPlayFile = NULL;
	gchar *pShuffleState = NULL;
	gchar *pRepeatState = NULL;
	gchar *pCurrentProgress = NULL;
	gchar *pProgressBarPlayState = NULL;
	gint inResult;
	gboolean bPlayState;
	gdouble progress;
	GHashTable *pPropertyHash;
	GPtrArray *gpArray;

	g_object_get(defaultVideoPlayer->priv->player,"progress",&progress,NULL);
	pCurrentProgress = g_strdup_printf("%f",progress);
	pCurPlayFile = g_strdup_printf("%d",defaultVideoPlayer->priv->currentPlayingFile);
	pRepeatState = g_strdup_printf("%d",defaultVideoPlayer->priv->isRepeat);
	pShuffleState = g_strdup_printf("%d",defaultVideoPlayer->priv->isShuffle);

	pPropertyHash = thornbury_get_property (defaultVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "play-state", NULL);
	if(NULL != pPropertyHash )
	{
		GValue *playState = NULL;
		playState = g_hash_table_lookup(pPropertyHash , "play-state");
		if(NULL != playState)
		{
			bPlayState = g_value_get_boolean(playState);
		}
	}
	pProgressBarPlayState = g_strdup_printf("%d",bPlayState);

	gpArray = seaton_preference_get(defaultVideoPlayer->priv->sqliteObj,NULL, NULL,NULL);
	if(gpArray != NULL)
	{
		seaton_preference_remove(defaultVideoPlayer->priv->sqliteObj,NULL, NULL,NULL);
	}

	if(defaultVideoPlayer->priv->pHeaderText)
	{
		/* GHashTable isn't const-correct for const keys, hence
		 * all these casts */
		g_hash_table_insert (defaultVideoPlayer->priv->pExitStateHash,
			(gchar *) "CurrentPlayingFile", pCurPlayFile);
		g_hash_table_insert (defaultVideoPlayer->priv->pExitStateHash,
			(gchar *) "RepeatState", pRepeatState);
		g_hash_table_insert (defaultVideoPlayer->priv->pExitStateHash,
			(gchar *) "ShuffleState", pShuffleState);
		g_hash_table_insert (defaultVideoPlayer->priv->pExitStateHash,
			(gchar *) "HeaderText", defaultVideoPlayer->priv->pHeaderText);
		g_hash_table_insert (defaultVideoPlayer->priv->pExitStateHash,
			(gchar *) "PlayingDuration", pCurrentProgress);
		g_hash_table_insert (defaultVideoPlayer->priv->pExitStateHash,
			(gchar *) "PlayState", pProgressBarPlayState);
		inResult = seaton_preference_add_data (defaultVideoPlayer->priv->sqliteObj,NULL,defaultVideoPlayer->priv->pExitStateHash);
	}
	if( !inResult )
	{
		//	g_print("\n LastUserMode Data Added ..\n");
	}
	else if (inResult)
	{
		//	g_print("\n LastUserMode Data Adding Failed ...\n");
	}
	g_hash_table_destroy(defaultVideoPlayer->priv->pExitStateHash);

	exit(0);
}



/*********************************************************************************************
 * Function: main
 * Description: main function of the VideoPlayer
 * Parameters:  argc,argv
 * Return:   int
 ********************************************************************************************/
static void
video_player_exit_functionalities (void)
{
	struct sigaction action;
	action.sa_handler = video_player_termination_handler;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGTERM, &action, NULL);

}



/*********************************************************************************************
 * Function: main
 * Description: main function of the VideoPlayer
 * Parameters:  argc,argv
 * Return:   int
 ********************************************************************************************/
int main( gint argc, gchar **argv )
{
	const char *env_string;
	ClutterActor *pVideoPlayer = NULL;
	EyeVideoPlayer *pVideoPlayerPtr;
	const gchar *app_id = EYE_APP_ID;

	clutter_gst_init(&argc, &argv);
	initialize_ui_toolkit(argc , argv);

	env_string = g_getenv ("EYE_VIDEOPLAYER_DEBUG");
	if (env_string != NULL)
	{
		videoplayer_debug_flags =    g_parse_debug_string (env_string,  videoplayer_debug_keys,  G_N_ELEMENTS (videoplayer_debug_keys));
		EYE_VIDEOPLAYER_DEBUG ("\nenv_string %s %d %d \n", env_string,
			VIDEOPLAYER_HAS_DEBUG,
			videoplayer_debug_flags);
	}


	if(NULL != argv)
	{
		gint i = 0;
		while (argv[i])
		{
			if(g_strcmp0(argv[i],"app-name") == 0)
			{
				app_id = argv[i + 1];
			}
			if(g_strcmp0(argv[i],"url") == 0)
			{
				if(g_strcmp0(argv[i+1],"NULL") == 0)
					pURL = NULL;
				else
					pURL = g_strdup(argv[i+1]);	
			}
			i++;
		}
	}

	pVideoPlayerStage = create_mildenhall_window (app_id);

	pVideoPlayer = eye_create (app_id);

	clutter_actor_add_child (CLUTTER_ACTOR (pVideoPlayerStage),pVideoPlayer);
	pVideoPlayerPtr = EYE_VIDEO_PLAYER (pVideoPlayer);

	pVideoPlayerPtr->priv->bPlayState = FALSE;
	if(NULL != argv)
	{
		gint i = 0;
		while (argv[i])
		{
			if(g_strcmp0(argv[i],"play-mode") == 0)
			{
				if(g_strcmp0(argv[i + 1],"play") == 0)
				{
					pVideoPlayerPtr->priv->bPlayState = TRUE;
					break;
				}
			}
			i++;
		}
	}

	video_player_create_exit_state_pdi(pVideoPlayerPtr);
	video_player_exit_functionalities();
	initialize_client_handlers(pVideoPlayer);
	
	clutter_main ();
	return TRUE;
}

/*********************************************************************************************
 * Function: initialize_client_handlers
 * Description: Initialize the client handlers.Calls a function defined in client_handler.c
 * Parameters:  actor
 * Return:   void
 ********************************************************************************************/
static void initialize_client_handlers(ClutterActor* actor)
{
	initialize_app_manager_client_handler(actor);
}

/*********************************************************************************************
 * Function: eye_get_property
 * Description: Get the Properties
 * Parameters:  object,property_id,value,pspec
 * Return:   void
 ********************************************************************************************/
static void eye_get_property (GObject *object,guint property_id,GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/*********************************************************************************************
 * Function: eye_set_property
 * Description: Set the Properties
 * Parameters:  object,property_id,value,pspec
 * Return:   void
 ********************************************************************************************/
static void eye_set_property (GObject *object,guint property_id,const GValue *value,GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/*********************************************************************************************
 * Function: eye_dispose
 * Description: VideoPlayer dispose function
 * Parameters:  object
 * Return:   void
 ********************************************************************************************/
static void eye_dispose (GObject *object)
{
	EyeVideoPlayer *self = EYE_VIDEO_PLAYER (object);

	g_clear_object (&self->priv->tracker);

	G_OBJECT_CLASS (eye_parent_class)->dispose (object);
}

/*********************************************************************************************
 * Function: eye_finalize
 * Description: VideoPlayer finalize function
 * Parameters:  object
 * Return:   void
 ********************************************************************************************/
static void eye_finalize (GObject *object)
{
	G_OBJECT_CLASS (eye_parent_class)->finalize (object);
}

/*
 * eye_create:
 * @app_id: the app ID, either %EYE_APP_ID for the normal Eye instance
 *  or something other than %EYE_APP_ID for instances launched for removable
 *  media
 *
 * Create the video player actor.
 *
 * Returns: the #EyeVideoPlayer
 */
ClutterActor *
eye_create (const gchar *app_id)
{
	EyeVideoPlayer *pVideoPlayer = g_object_new (EYE_TYPE_VIDEO_PLAYER,
		NULL);
	defaultVideoPlayer = pVideoPlayer;
	pVideoPlayer->priv->EYE_APP_NAME = g_strdup (app_id);
	eye_init_ui(pVideoPlayer);

	return CLUTTER_ACTOR(pVideoPlayer);

}

/*********************************************************************************************
 * Function: eye_class_init
 * Description: ClassInit of VideoPlayer
 * Parameters:  EyeVideoPlayerClass
 * Return:   void
 ********************************************************************************************/
static void eye_class_init (EyeVideoPlayerClass *klass)
{

	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	g_type_class_add_private (klass, sizeof (EyeVideoPlayerPrivate));
	object_class->get_property = eye_get_property;
	object_class->set_property = eye_set_property;
	object_class->dispose = eye_dispose;
	object_class->finalize = eye_finalize;
}

/*********************************************************************************************
 * Function: eye_init
 * Description: Init of VideoPlayer
 * Parameters:  EyeVideoPlayer
 * Return:   void
 ********************************************************************************************/
static void eye_init (EyeVideoPlayer *self)
{
	self->priv = EYE_PRIVATE (self);

	self->priv->tracker = grassmoor_tracker_new ();
}
