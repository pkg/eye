/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**********************************************************************
              				Header Files
 ***********************************************************************/
#include "eye.h"

extern EyeVideoPlayer *defaultVideoPlayer;
extern gchar *pURL;
static void v_eye_request_for_thumbnails(gpointer *pUserData);


/*********************************************************************************************
 * Function: set_video_player_progress_bar
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void set_video_player_progress_bar(GObject *pObject, GParamSpec *pspec,gpointer data)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (data);
	gdouble progress;
	ClutterActor *videoActor;

	EYE_VIDEOPLAYER_DEBUG ("\n set_video_player_progress_bar \n");
	g_object_get(pVideoPlayer->priv->player, "video-actor", &videoActor, NULL);
	if(CLUTTER_ACTOR_IS_VISIBLE(CLUTTER_ACTOR(videoActor)))
	{
		//	g_print("\n PLAYER OBJECT IS VISIBLE \n");
	}
	else
	{

		clutter_actor_show(CLUTTER_ACTOR(videoActor));   //FIXME: (Roller issue).Need to remove after solving that issue. 
	}
	//If seek started then do not update the position of the progress bar.
	if(pVideoPlayer->priv->seekStart == FALSE)
	{
		//Set the progress bar position.
		g_object_get(pVideoPlayer->priv->player,"progress",&progress,NULL);

		update_video_player_progress_and_bottom_bar(progress);
	}

}

/*********************************************************************************************
 * Function: media_end_of_stream
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void media_end_of_stream(GObject *pObject, GParamSpec *pspec,gpointer data)
{
	GValue pValue = { 0, };
	const gchar *Line = NULL;
	GValue nullValue = { 0, };
	const gchar *pLine;
	GrassmoorMediaInfo *MedStruct;

	EYE_VIDEOPLAYER_DEBUG ("\n media_end_of_stream \n ");

	g_value_init (&pValue, G_TYPE_BOOLEAN);
	g_value_set_boolean (&pValue, TRUE);
	g_value_init (&nullValue, G_TYPE_BOOLEAN);
	g_value_set_boolean (&nullValue, FALSE);

	if(defaultVideoPlayer->priv->prevPlayingFile >=0)
	{
		//Here disconnect the signals for player object and pause
		thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,defaultVideoPlayer->priv->prevPlayingFile,2,&nullValue);
		g_signal_handlers_disconnect_by_func(defaultVideoPlayer->priv->player,G_CALLBACK(media_end_of_stream),defaultVideoPlayer);
		g_signal_handlers_disconnect_by_func(defaultVideoPlayer->priv->player,G_CALLBACK(set_video_player_progress_bar),defaultVideoPlayer);
	}
	//If Repeat state is True.(If both repeat and shuffle is true then priority has given to repeat.
	if(defaultVideoPlayer->priv->isRepeat)
	{
		EYE_VIDEOPLAYER_DEBUG("\n Repeat state is TRUE and playing file = %d",defaultVideoPlayer->priv->currentPlayingFile);
		g_signal_connect (defaultVideoPlayer->priv->player, "notify::progress", G_CALLBACK (set_video_player_progress_bar), defaultVideoPlayer);
		g_signal_connect (defaultVideoPlayer->priv->player, "eos", G_CALLBACK (media_end_of_stream), defaultVideoPlayer);
		//Get the file which has to be played.
		Line = g_ptr_array_index(defaultVideoPlayer->priv->pVideoUriList,defaultVideoPlayer->priv->currentPlayingFile);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,defaultVideoPlayer->priv->currentPlayingFile,2,&pValue);
		g_object_set (defaultVideoPlayer->priv->player, "uri", Line, NULL);
		g_object_set(defaultVideoPlayer->priv->player,"playing",TRUE, NULL);
		defaultVideoPlayer->priv->prevPlayingFile = defaultVideoPlayer->priv->currentPlayingFile;
	}
	else
	{
		//If shuffle state is true
		if(defaultVideoPlayer->priv->isShuffle)
		{
			//Generate the random number in the range of 0 to number of video files.
			gint32 shuffleIndex = g_rand_int_range(g_rand_new(),0,defaultVideoPlayer->priv->pVideoUriList->len);
			ThornburyModelIter *pIter;
			GValue playerObject = { 0, };

			EYE_VIDEOPLAYER_DEBUG ("\n Shuffle state is TRUE and ShuffleIndex = %d", shuffleIndex);
			pIter = thornbury_model_get_iter_at_row (defaultVideoPlayer->priv->pThumbRollerModel, shuffleIndex);
			thornbury_model_iter_get_value(pIter,1,&playerObject);
			defaultVideoPlayer->priv->player = g_value_get_object(&playerObject);
			g_signal_connect (defaultVideoPlayer->priv->player, "notify::progress", G_CALLBACK (set_video_player_progress_bar), defaultVideoPlayer);
			g_signal_connect (defaultVideoPlayer->priv->player, "eos", G_CALLBACK (media_end_of_stream), defaultVideoPlayer);
			Line = g_ptr_array_index(defaultVideoPlayer->priv->pVideoUriList,shuffleIndex);
			thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,shuffleIndex,2,&pValue);
			g_object_set (defaultVideoPlayer->priv->player, "uri", Line, NULL);
			g_object_set(defaultVideoPlayer->priv->player,"playing",TRUE, NULL);
			defaultVideoPlayer->priv->currentPlayingFile = shuffleIndex;
			defaultVideoPlayer->priv->prevPlayingFile = defaultVideoPlayer->priv->currentPlayingFile;
		}
		else
		{
			//If neither repeat nor shuffle is true.
			++defaultVideoPlayer->priv->currentPlayingFile;
			//Give next file for playing.And if it is a last file play the first file.
			if(defaultVideoPlayer->priv->currentPlayingFile <= (defaultVideoPlayer->priv->pVideoUriList->len-1))
			{
				ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(defaultVideoPlayer->priv->pThumbRollerModel, defaultVideoPlayer->priv->currentPlayingFile);
				GValue playerObject = { 0, };
				thornbury_model_iter_get_value(pIter,1,&playerObject);
				defaultVideoPlayer->priv->player = g_value_get_object(&playerObject);
				g_signal_connect (defaultVideoPlayer->priv->player, "notify::progress", G_CALLBACK (set_video_player_progress_bar), defaultVideoPlayer);
				g_signal_connect (defaultVideoPlayer->priv->player, "eos", G_CALLBACK (media_end_of_stream), defaultVideoPlayer);

				Line = g_ptr_array_index(defaultVideoPlayer->priv->pVideoUriList,defaultVideoPlayer->priv->currentPlayingFile);
				thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,defaultVideoPlayer->priv->currentPlayingFile,2,&pValue);
				g_object_set (defaultVideoPlayer->priv->player, "uri", Line, NULL);
				g_object_set(defaultVideoPlayer->priv->player,"playing",TRUE, NULL);
				defaultVideoPlayer->priv->prevPlayingFile = defaultVideoPlayer->priv->currentPlayingFile;
			}
			if(defaultVideoPlayer->priv->currentPlayingFile > (defaultVideoPlayer->priv->pVideoUriList->len - 1))	//If it is a last file play the first file.
			{
				ThornburyModelIter *pIter;
				GValue playerObject = { 0, };

				defaultVideoPlayer->priv->currentPlayingFile = 0;
				pIter = thornbury_model_get_iter_at_row (
					defaultVideoPlayer->priv->pThumbRollerModel,
					defaultVideoPlayer->priv->currentPlayingFile);
				thornbury_model_iter_get_value(pIter,1,&playerObject);
				defaultVideoPlayer->priv->player = g_value_get_object(&playerObject);
				g_signal_connect (defaultVideoPlayer->priv->player, "notify::progress", G_CALLBACK (set_video_player_progress_bar), defaultVideoPlayer);
				g_signal_connect (defaultVideoPlayer->priv->player, "eos", G_CALLBACK (media_end_of_stream), defaultVideoPlayer);

				Line = g_ptr_array_index(defaultVideoPlayer->priv->pVideoUriList,defaultVideoPlayer->priv->currentPlayingFile);
				thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,defaultVideoPlayer->priv->currentPlayingFile,2,&pValue);
				g_object_set (defaultVideoPlayer->priv->player, "uri", Line, NULL);
				g_object_set(defaultVideoPlayer->priv->player,"playing",TRUE, NULL);
				defaultVideoPlayer->priv->prevPlayingFile = defaultVideoPlayer->priv->currentPlayingFile;
			}
		}

	}
	//Set current playing video item to focus.(List and Thumb roller). 
	thornbury_set_property(defaultVideoPlayer->priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER, "focused-row", defaultVideoPlayer->priv->currentPlayingFile, NULL);
	thornbury_set_property(defaultVideoPlayer->priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row", defaultVideoPlayer->priv->currentPlayingFile, NULL);
	update_detail_view_header (Line);

	pLine = g_ptr_array_index (defaultVideoPlayer->priv->pVideoUriList,
		defaultVideoPlayer->priv->currentPlayingFile);
	/* FIXME: Add error handling */
	MedStruct = grassmoor_tracker_get_media_file_info (defaultVideoPlayer->priv->tracker, pLine,
		GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO , NULL);

	update_detail_meta_roller(MedStruct);
	if(MedStruct != NULL)
		grassmoor_media_info_free (MedStruct);

}
/*********************************************************************************************
 * Function: extract_video_info_from_meta_tracker
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void extract_video_info_from_meta_tracker(EyeVideoPlayer *pVideoPlayer)
{
	const gchar *Line;
	GrassmoorMediaInfo *MedStruct = NULL;
	gint totalFiles = 0;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	EYE_VIDEOPLAYER_DEBUG("\n extract_video_info_from_meta_tracker \n ");

	if (g_strcmp0 (pVideoPlayer->priv->EYE_APP_NAME, EYE_APP_ID) == 0)
	{
		/* FIXME: Add error handling */
		pVideoPlayer->priv->pVideoUriList = grassmoor_tracker_get_url_list (pVideoPlayer->priv->tracker, GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO,
			GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_ASC, 0, 1000, NULL);
	}
	else
	{
		/* FIXME: Add error handling */
		pVideoPlayer->priv->pVideoUriList = grassmoor_tracker_get_url_list (pVideoPlayer->priv->tracker, GRASSMOOR_TRACKER_VOLUME_REMOVABLE, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO,
			GRASSMOOR_TRACKER_MEDIA_INFO_URL, GRASSMOOR_TRACKER_ORDER_TYPE_ASC, 0, 1000, NULL);
	}

	//Add dummy Items to the roller.
	while(totalFiles < pVideoPlayer->priv->pVideoUriList->len)
	{
		add_items_to_thumb_roller();
		totalFiles++;
	}
	//FIXME: To solve the roller problem (Multiple of 4 items).
	pVideoPlayer->priv->pExtraThumbs = thornbury_model_get_n_rows(pVideoPlayer->priv->pThumbRollerModel) % 4 ;
	if(pVideoPlayer->priv->pExtraThumbs > 0)
	  pVideoPlayer->priv->pExtraThumbs = 4 - pVideoPlayer->priv->pExtraThumbs;
	add_remove_thumbs_divisible_by_4(TRUE);

	if(pVideoPlayer->priv->pVideoUriList->len)
	{
		Line = g_ptr_array_index(pVideoPlayer->priv->pVideoUriList,0);
		/* FIXME: Add error handling */
		MedStruct = grassmoor_tracker_get_media_file_info (pVideoPlayer->priv->tracker, Line, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO, NULL);
	}

	if(MedStruct)
	{
		gchar  **path, **mim;
		path = g_new0 (gchar*, 1);
		mim = g_new0 (gchar*, 1);
		if(Line)
			path[0] = g_strdup (Line);
		mim[0] =  g_strdup (grassmoor_media_info_get_content_type (MedStruct));
		path[1] = NULL;
		mim[1] = NULL;
		//Requesting thumbnail for the first video.(Logic has to be changed[Need to check for is file exists or not]). 
		g_thread_new("ThumbNailThread",(GThreadFunc)v_eye_request_for_thumbnails,(gpointer)pVideoPlayer);
		grassmoor_media_info_free (MedStruct);
	}
}

/*********************************************************************************************
 * Function:    v_vrns_video_player_request_for_thumbnails 
 * Description: 
 * Parameters:  VrnsVideoPlayer
 * Return:      void
 ********************************************************************************************/
static void v_eye_request_for_thumbnails(gpointer *pUserData)
{
        EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
        gint index = 0;
        gchar **pThumbNailPath = NULL;
        pThumbNailPath = g_new0(gchar*,1);
        while(index < pVideoPlayer->priv->pVideoUriList->len)
        {
              gchar *pThumbPath = g_strdup (grassmoor_tracker_generate_thumbnail_uri (g_ptr_array_index (pVideoPlayer->priv->pVideoUriList, index)));
                pThumbNailPath[index] = pThumbPath;
                pThumbNailPath = g_renew(gchar* , pThumbNailPath, index+2);
                index++;
        }
 	g_timeout_add(50,eye_update_thumb_list,pThumbNailPath);
 	g_timeout_add(50,eye_update_list_roller,pThumbNailPath);
}


/*********************************************************************************************
 * Function: eye_view_switch_begin_cb
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void eye_view_switch_begin_cb(ThornburyViewManager *pThornburyViewManager, gchar *pName, gpointer *pUserData)
{
	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pUserData));


	if(g_strcmp0(pName, "DetailView" ) == 0)
	{
		update_views_drawer(DETAIL_VIEW);
	}
	else if(g_strcmp0(pName, "ThumbView" ) == 0)
	{
		update_views_drawer(THUMB_VIEW);
	}
	else if(g_strcmp0(pName,"ListView") == 0)
	{
		update_views_drawer(LIST_VIEW);
	}
        
	if(g_strcmp0(pName, "ThumbView" ) == 0 && defaultVideoPlayer->priv->bRemoveState == FALSE)
	{
                add_remove_thumbs_divisible_by_4(TRUE);
	}
}

static gboolean
focus_thumb (gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row", pVideoPlayer->priv->currentPlayingFile, NULL);
	return FALSE;
}
/*********************************************************************************************
 * Function: eye_view_switched
 * Description: callback for view switch
 * Parameters:  ThornburyViewManager *, pUserData
 * Return:   void
 ********************************************************************************************/
void eye_view_switched(ThornburyViewManager *pThornburyViewManager,gpointer pName, gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	gchar *viewName =(gchar *)pName;
	
	if(g_strcmp0(viewName, "DetailView" ) == 0 && pVideoPlayer->priv->bRemoveState == TRUE)
	{
                add_remove_thumbs_divisible_by_4(FALSE);
		g_timeout_add(500,focus_thumb,pVideoPlayer);
	} else if (g_strcmp0(viewName, "DetailView") == 0
			&& pVideoPlayer->priv->bRemoveState == FALSE) {
		focus_thumb(pVideoPlayer);
	}
	
	if (!g_strcmp0(viewName, "ListView"))
	{
		pVideoPlayer->priv->pCurrentView = LIST_VIEW ;
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager,
				LIST_VIEW_LIST_ROLLER, "refresh", TRUE, NULL);
	}
	else if(!g_strcmp0(viewName, "ThumbView"))
	{
		pVideoPlayer->priv->pCurrentView = THUMB_VIEW ;
	}
	else if(!g_strcmp0(viewName, "DetailView"))
	{
		pVideoPlayer->priv->pCurrentView = DETAIL_VIEW ;
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "width", 375.0, NULL);
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "opacity", 255, NULL);
	}
	else if (!g_strcmp0(viewName, "FullScreenView"))
	{
		pVideoPlayer->priv->pCurrentView = FULL_SCREEN_VIEW ;
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "width", 573.0, NULL);
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "opacity", 75, NULL);
	}
}

/*********************************************************************************************
 * Function: handle_audio_response
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void handle_audio_response( CanterburyAudioMgr *object, const gchar *arg_app_name, guint arg_audio_result , gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	if( g_strcmp0(pVideoPlayer->priv->EYE_APP_NAME , arg_app_name) == FALSE )
	{
		if(arg_audio_result == CANTERBURY_AUDIO_PLAY)
		{
			const gchar *Line;
			GValue pValue = { 0, };
			GValue nullValue = { 0, };
			ThornburyModelIter *pIter;
			GValue playerObject = { 0, };

			g_value_init (&pValue, G_TYPE_BOOLEAN);
			g_value_set_boolean (&pValue, TRUE);

			g_value_init (&nullValue, G_TYPE_BOOLEAN);
			g_value_set_boolean (&nullValue, FALSE);

			pIter = thornbury_model_get_iter_at_row (
				pVideoPlayer->priv->pThumbRollerModel,
				pVideoPlayer->priv->currentPlayingFile);
			thornbury_model_iter_get_value(pIter,1,&playerObject);
			if(pVideoPlayer->priv->prevPlayingFile != pVideoPlayer->priv->currentPlayingFile)
			{
				EYE_VIDEOPLAYER_DEBUG("\n handle_audio_response : Clicked on Different Item \n ");
				if(pVideoPlayer->priv->prevPlayingFile >=0)
				{
					thornbury_model_insert_value(pVideoPlayer->priv->pThumbRollerModel,pVideoPlayer->priv->prevPlayingFile,2,&nullValue);
					g_signal_handlers_disconnect_by_func(pVideoPlayer->priv->player,G_CALLBACK(media_end_of_stream),pVideoPlayer);
					g_signal_handlers_disconnect_by_func(pVideoPlayer->priv->player,G_CALLBACK(set_video_player_progress_bar),pVideoPlayer);
					g_object_set(pVideoPlayer->priv->player,"playing",FALSE, NULL);
				}
				pVideoPlayer->priv->player = g_value_get_object(&playerObject);
				g_signal_connect (pVideoPlayer->priv->player, "notify::progress", G_CALLBACK (set_video_player_progress_bar), pVideoPlayer);
				g_signal_connect (pVideoPlayer->priv->player, "eos", G_CALLBACK (media_end_of_stream), pVideoPlayer);
				Line = g_ptr_array_index(pVideoPlayer->priv->pVideoUriList,pVideoPlayer->priv->currentPlayingFile);
				thornbury_model_insert_value(pVideoPlayer->priv->pThumbRollerModel,pVideoPlayer->priv->currentPlayingFile,2,&pValue);
				g_object_set (pVideoPlayer->priv->player, "uri", Line, NULL);
				g_object_set(pVideoPlayer->priv->player,"playing",TRUE, NULL);
				g_object_set(pVideoPlayer->priv->player,"progress",(gdouble)pVideoPlayer->priv->playingDuration,NULL);
				thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "play-state",TRUE, NULL);
				pVideoPlayer->priv->playingDuration = 0.0;
				pVideoPlayer->priv->prevPlayingFile = pVideoPlayer->priv->currentPlayingFile;
				update_detail_view_header (Line);
			}
			else
			{
				Line = g_ptr_array_index(pVideoPlayer->priv->pVideoUriList,pVideoPlayer->priv->currentPlayingFile);
				update_detail_view_header (Line);
				EYE_VIDEOPLAYER_DEBUG("\n handle_audio_response : Clicked on the Same  Item(Full Screen feature has to be handled \n ");
				g_object_set(pVideoPlayer->priv->player,"playing",TRUE, NULL);
				thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "play-state",TRUE, NULL);
			}
		}
		else if ( arg_audio_result == CANTERBURY_AUDIO_PAUSE )
		{
			g_object_set(pVideoPlayer->priv->player,"playing",FALSE, NULL);
			thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "play-state",FALSE, NULL);
		}
	}
}

/*********************************************************************************************
 * Function: eye_thumb_roller_item_activated_cb
 * Description: ThumbRoller Item activated callback function
 * Parameters:  pRoller , inRow , pUserData
 * Return:   void
 ********************************************************************************************/

void
eye_thumb_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	const gchar *pLine;

	pVideoPlayer->priv->currentPlayingFile = inRow;

	if(pVideoPlayer->priv->currentPlayingFile >= pVideoPlayer->priv->pVideoUriList->len )
		return;

	if( (CANTERBURY_IS_AUDIO_MGR (pVideoPlayer->priv->audio_mgr_proxy)))
	{
//		g_print("\n Thumb activated = %d",inRow);
		EYE_VIDEOPLAYER_DEBUG("\n Thumb roller item activated = %d \n ",pVideoPlayer->priv->currentPlayingFile);
		canterbury_audio_mgr_call_audio_request(pVideoPlayer->priv->audio_mgr_proxy , pVideoPlayer->priv->EYE_APP_NAME , NULL , NULL , NULL );
	}
	pLine = g_ptr_array_index (pVideoPlayer->priv->pVideoUriList,
		pVideoPlayer->priv->currentPlayingFile);
	/* FIXME: Add error handling */
	GrassmoorMediaInfo *MedStruct = grassmoor_tracker_get_media_file_info (pVideoPlayer->priv->tracker, pLine, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO, NULL);
	if(pVideoPlayer->priv->pCurrentView == THUMB_VIEW)
	{
		thornbury_switch_view(pVideoPlayer->priv->pThornburyViewManager,"DetailView",TRUE,pVideoPlayer->priv->currentPlayingFile);
		update_detail_meta_roller(MedStruct);
	}
	else if(pVideoPlayer->priv->pCurrentView == DETAIL_VIEW && pVideoPlayer->priv->currentPlayingFile == pVideoPlayer->priv->prevPlayingFile )
	{
		//FIXME:Commented for 24-May-2013 release.
#if 1
		GHashTable *anim = g_hash_table_new(g_str_hash,g_str_equal);
		ThornburyViewManagerViewSwitchAnimData *animData = g_new0(ThornburyViewManagerViewSwitchAnimData,1);
		animData->view_name = "DetailView";
		animData->exit_effect = "full_screen";
		animData->entry_effect = NULL;
		g_hash_table_insert (anim, (gchar *) "DetailView", animData);
		thornbury_set_view_switch_animation(pVideoPlayer->priv->pThornburyViewManager,anim);
#endif
		thornbury_switch_view(pVideoPlayer->priv->pThornburyViewManager,"FullScreenView",TRUE,pVideoPlayer->priv->currentPlayingFile);
	}
	else
	{
		update_detail_meta_roller(MedStruct);
	}
	thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER, "focused-row", pVideoPlayer->priv->currentPlayingFile, NULL);
	if(MedStruct != NULL)
		grassmoor_media_info_free (MedStruct);
}


/*********************************************************************************************
 * Function: eye_list_roller_item_activated_cb
 * Description: ListRoller Item activated callback function
 * Parameters:  pRoller , inRow , pUserData
 * Return:   void
 ********************************************************************************************/
void
eye_list_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData)
{

	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	pVideoPlayer->priv->currentPlayingFile = inRow;
	if( (CANTERBURY_IS_AUDIO_MGR (pVideoPlayer->priv->audio_mgr_proxy)))
	{
		const gchar *pLine;
		GrassmoorMediaInfo *MedStruct;

		EYE_VIDEOPLAYER_DEBUG("\n List roller item activated = %d \n ",pVideoPlayer->priv->currentPlayingFile);
		canterbury_audio_mgr_call_audio_request(pVideoPlayer->priv->audio_mgr_proxy , pVideoPlayer->priv->EYE_APP_NAME , NULL , NULL , NULL );
		thornbury_switch_view(pVideoPlayer->priv->pThornburyViewManager, "DetailView", TRUE,pVideoPlayer->priv->currentPlayingFile);

		pLine = g_ptr_array_index (pVideoPlayer->priv->pVideoUriList,
			pVideoPlayer->priv->currentPlayingFile);
		/* FIXME: Add error handling */
		MedStruct = grassmoor_tracker_get_media_file_info (pVideoPlayer->priv->tracker, pLine,
			GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO, NULL);
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row", pVideoPlayer->priv->currentPlayingFile, NULL);
		update_detail_meta_roller(MedStruct);

	}
}

/*********************************************************************************************
 * Function: eye_views_drawer_button_released_cb
 * Description: Views Drawer button released callback
 * Parameters:  pButton, pName,pUserData
 * Return:   void
 ********************************************************************************************/
void eye_views_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = pUserData;
	EyeVideoPlayerPrivate *priv;
	GHashTable *anim;
	ThornburyViewManagerViewSwitchAnimData *animData;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	priv = pVideoPlayer->priv;

	anim = g_hash_table_new (g_str_hash, g_str_equal);
	animData = g_new0 (ThornburyViewManagerViewSwitchAnimData, 1);
	animData->view_name = "DetailView";
	animData->exit_effect = NULL;
	animData->entry_effect = NULL;
	g_hash_table_insert (anim, (gchar *) "DetailView", animData);
	thornbury_set_view_switch_animation(priv->pThornburyViewManager,anim);


	if (!g_strcmp0(pName, "LIST-VIEW"))
	{

		thornbury_set_property(priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER, "focused-row", priv->currentPlayingFile, NULL);
		thornbury_switch_view_no_animation(priv->pThornburyViewManager, "ListView", TRUE,pVideoPlayer->priv->currentPlayingFile);
	}
	else if (!g_strcmp0(pName, "THUMB-VIEW"))
	{
		thornbury_switch_view_no_animation(priv->pThornburyViewManager, "ThumbView", TRUE,pVideoPlayer->priv->currentPlayingFile);
	}
	else if (!g_strcmp0(pName, "DETAIL-VIEW"))
	{
		GHashTable *pPropertyHash;

		thornbury_switch_view(priv->pThornburyViewManager, "DetailView", TRUE,pVideoPlayer->priv->currentPlayingFile);
		pPropertyHash = thornbury_get_property (priv->pThornburyViewManager,
			THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row", NULL);
		if(NULL != pPropertyHash )
		{
			GValue *FocussedRow = NULL;
			FocussedRow = g_hash_table_lookup(pPropertyHash , "focused-row");
			if(NULL != FocussedRow)
			{
				//guint inFocusRow = g_value_get_int(FocussedRow);
				//	g_print("\n Focused Row = %d",inFocusRow);
			}
		}

	}
}

/*********************************************************************************************
 * Function: eye_thumb_context_drawer_button_released_cb
 * Description: Context Drawer button released callback
 * Parameters:  pButton, pName,pUserData
 * Return:   void
 ********************************************************************************************/
void eye_thumb_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData)
{

}

/*********************************************************************************************
 * Function: eye_full_screen_left_context_drawer_button_released_cb
 * Description: Context Drawer button released callback
 * Parameters:  pButton, pName,pUserData
 * Return:   void
 ********************************************************************************************/
void eye_full_screen_left_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = pUserData;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));

	EYE_VIDEOPLAYER_DEBUG("\n Play Requested \n ");
	thornbury_switch_view(pVideoPlayer->priv->pThornburyViewManager,"DetailView",TRUE,pVideoPlayer->priv->currentPlayingFile);
}


/*********************************************************************************************
 * Function: eye_full_screen_right_context_drawer_button_released_cb
 * Description: Context Drawer button released callback
 * Parameters:  pButton, pName,pUserData
 * Return:   void
 ********************************************************************************************/
void eye_full_screen_right_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData)
{

}


/*********************************************************************************************
 * Function: progress_bar_play_requested_cb
 * Description:
 * Parameters:
 * Return:   gboolean
 ********************************************************************************************/
gboolean progress_bar_play_requested_cb(MildenhallProgressBar *progressBar, gpointer userData)
{
	EyeVideoPlayer *pVideoPlayer = userData;

	g_return_val_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer), FALSE);

	EYE_VIDEOPLAYER_DEBUG("\n Play Requested \n ");
	pVideoPlayer->priv->progressBarPlayState = TRUE;
	if( (CANTERBURY_IS_AUDIO_MGR (pVideoPlayer->priv->audio_mgr_proxy)))
	{
		canterbury_audio_mgr_call_audio_request(pVideoPlayer->priv->audio_mgr_proxy , pVideoPlayer->priv->EYE_APP_NAME , NULL , NULL , NULL );
	}

	return TRUE;
}

/*********************************************************************************************
 * Function: progress_bar_seek_end_cb
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
gboolean progress_bar_seek_end_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer userData)
{
	EyeVideoPlayer *pVideoPlayer = userData;

	g_return_val_if_fail (EYE_IS_VIDEO_PLAYER (userData), FALSE);

	EYE_VIDEOPLAYER_DEBUG("\n progress_bar_seek_end_cb \n ");
	pVideoPlayer->priv->seekStart = FALSE;
	g_object_set(pVideoPlayer->priv->player, "progress",(gdouble)position,NULL);
	return TRUE;
}

/*********************************************************************************************
 * Function: progress_bar_seek_started_cb
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
gboolean progress_bar_seek_started_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer userData)
{
	EyeVideoPlayer *pVideoPlayer = userData;

	g_return_val_if_fail (EYE_IS_VIDEO_PLAYER (userData), FALSE);

	EYE_VIDEOPLAYER_DEBUG("\n progress_bar_seek_started_cb \n ");
	pVideoPlayer->priv->seekStart = TRUE;
	return TRUE;
}

/*********************************************************************************************
 * Function: progress_bar_pause_requested_cb
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
gboolean progress_bar_pause_requested_cb(MildenhallProgressBar *progressBar, gpointer userData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (userData);

	EYE_VIDEOPLAYER_DEBUG ("\n progress_bar_pause_requested_cb\n");
	pVideoPlayer->priv->progressBarPlayState = FALSE;
	g_object_set(pVideoPlayer->priv->player,"playing",FALSE, NULL);

	return TRUE;
}


/*********************************************************************************************
 * Function: progress_bar_seek_updated_cb
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
gboolean progress_bar_seek_updated_cb(MildenhallProgressBar *progressBar, gpointer userData)
{
	return TRUE;
}



/*********************************************************************************************
 * Function: bottom_bar_pressed
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
gboolean
bottom_bar_pressed (MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData)
{
	return TRUE;
}


/*********************************************************************************************
 * Function: bottom_bar_released
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
gboolean
bottom_bar_released (MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	if (!g_strcmp0(pButtonName, REPEAT))
	{
		if(pVideoPlayer->priv->isRepeat)
		{
			pVideoPlayer->priv->isRepeat = FALSE;
		}
		else
		{
			pVideoPlayer->priv->isRepeat = TRUE;
		}
	}

	if(!g_strcmp0(pButtonName,SHUFFLE))
	{
		if(pVideoPlayer->priv->isShuffle)
		{
			pVideoPlayer->priv->isShuffle = FALSE;
		}
		else
		{
			pVideoPlayer->priv->isShuffle = TRUE;
		}
	}
	return TRUE;
}


/*********************************************************************************************
 * Function: video_player_create_exit_state_pdi
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void video_player_create_exit_state_pdi(EyeVideoPlayer *pVideoPlayer)
{
	gint inResult;
	SeatonFieldParam pfieldparam[10];
	pVideoPlayer->priv->sqliteObj=seaton_preference_new();
	inResult = seaton_preference_open (
		pVideoPlayer->priv->sqliteObj,
		EYE_BUNDLE_ID,
		pVideoPlayer->priv->EYE_APP_NAME,
		SEATON_PREFERENCE_APP_INTERNAL_DB);
	if( !inResult )
	{
		//	g_print("\n Video Player Data Base Created...\n");
	}
	else if (inResult)
	{
		//g_print("\n Video Player Data Base Creation failed...\n");
	}
	pfieldparam[0].pKeyName = g_strdup("CurrentPlayingFile");
	pfieldparam[0].inFieldType= SQLITE_INTEGER;
	
	pfieldparam[1].pKeyName = g_strdup("RepeatState");
	pfieldparam[1].inFieldType= SQLITE_INTEGER;

	pfieldparam[2].pKeyName = g_strdup("ShuffleState");
	pfieldparam[2].inFieldType= SQLITE_INTEGER;

	pfieldparam[3].pKeyName = g_strdup("PlayingDuration");
	pfieldparam[3].inFieldType= SQLITE_INTEGER;

	pfieldparam[4].pKeyName = g_strdup("HeaderText");
	pfieldparam[4].inFieldType= SQLITE_TEXT;

	pfieldparam[5].pKeyName = g_strdup("PlayState");
	pfieldparam[5].inFieldType= SQLITE_INTEGER;

	inResult =  seaton_preference_install(pVideoPlayer->priv->sqliteObj,NULL,pfieldparam,6);

	if( !inResult )
	{
		//	g_print("\n Video Player Table creation Successful ..\n");
	}
	else if (inResult)
	{
		//	g_print("\n Video Player Table creation failed...\n");
	}

	if(NULL != pfieldparam[0].pKeyName)
	{
		g_free(pfieldparam[0].pKeyName);
		pfieldparam[0].pKeyName = NULL;
	}
	if(NULL != pfieldparam[1].pKeyName)
	{
		g_free(pfieldparam[1].pKeyName);
		pfieldparam[1].pKeyName = NULL;
	}
	if(NULL != pfieldparam[2].pKeyName)
	{
		g_free(pfieldparam[2].pKeyName);
		pfieldparam[2].pKeyName = NULL;
	}
	if(NULL != pfieldparam[3].pKeyName)
	{
		g_free(pfieldparam[3].pKeyName);
		pfieldparam[3].pKeyName = NULL;
	}
	if(NULL != pfieldparam[4].pKeyName)
	{
		g_free(pfieldparam[4].pKeyName);
		pfieldparam[4].pKeyName = NULL;
	}
	if(NULL != pfieldparam[5].pKeyName)
	{
		g_free(pfieldparam[5].pKeyName);
		pfieldparam[5].pKeyName = NULL;
	}
}

/*********************************************************************************************
 * Function: video_player_last_user_mode_handler
 * Description: Extracts the video meta information
 * Parameters:  pVideoPlayer
 * Return:   void
 ********************************************************************************************/
void video_player_last_user_mode_handler(EyeVideoPlayer *pVideoPlayer)
{
	ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(pVideoPlayer->priv->pThumbRollerModel, pVideoPlayer->priv->currentPlayingFile);
	GValue playerObject = { 0, };
	const gchar *Line;
	GValue pValue = { 0, };
	GrassmoorMediaInfo *MedStruct;

	thornbury_model_iter_get_value (pIter,1,&playerObject);
	g_value_init (&pValue, G_TYPE_BOOLEAN);
	g_value_set_boolean (&pValue, TRUE);

	if(pVideoPlayer->priv->isShuffle)
	{
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_BOTTOMBAR, "button-one-state", TRUE, NULL);
	}

	if(pVideoPlayer->priv->isRepeat)
	{
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_BOTTOMBAR, "button-two-state", TRUE, NULL);
	}

	if( (CANTERBURY_IS_AUDIO_MGR (pVideoPlayer->priv->audio_mgr_proxy)))
	{
		canterbury_audio_mgr_call_audio_request(pVideoPlayer->priv->audio_mgr_proxy , pVideoPlayer->priv->EYE_APP_NAME , NULL , NULL , NULL );
	}

	thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "current-duration",pVideoPlayer->priv->playingDuration, NULL);

	Line = g_ptr_array_index (pVideoPlayer->priv->pVideoUriList, pVideoPlayer->priv->currentPlayingFile);
	update_detail_view_header (Line);
	/* FIXME: Add error handling */
	MedStruct = grassmoor_tracker_get_media_file_info (pVideoPlayer->priv->tracker, Line,
		GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO, NULL);
	update_detail_meta_roller(MedStruct);

}

/**********************************************************************
              				END OF FILE
 ***********************************************************************/
